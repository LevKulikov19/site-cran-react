import React from "react";
import AppRouter from "./components/AppRouter";
import HeaderComponent from "./components/HeaderComponent/HeaderComponent";
import {BrowserRouter} from "react-router-dom";
import FooterComponent from "./components/FooterComponent/FooterComponent";
import { CartProvider } from "./context/CartContext"; 

function App() {
  return (
    <CartProvider>
      <BrowserRouter>
        <HeaderComponent />
        <AppRouter />
        <FooterComponent />
      </BrowserRouter>
    </CartProvider>
  )
}

export default App;

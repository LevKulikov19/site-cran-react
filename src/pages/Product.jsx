import React from 'react';
import { useParams } from 'react-router-dom';
import ProductComponent from '../components/ProductComponent/ProductComponent';
import { getById, getCategoryByProductId } from '../product/ProductService';
import classes from './Product.module.css';
import { Link } from 'react-router-dom';

const Product = () => {
    window.scrollTo(0, 0);
    const params = useParams();
    let product = getById(params.id);
    let category = getCategoryByProductId(params.id);
    return (
        <main className={classes['product-main-container'] + ' container-xl'}>
            <nav className="breadcrumb-main">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><Link to="/">Главная</Link></li>
                    <li className="breadcrumb-item"><Link to="/category">Каталог</Link></li>
                    <li className="breadcrumb-item"><Link to={category.CategoryLink}
                        className="breadcrumb-product-category">{category.Category}</Link></li>
                    <li className="breadcrumb-item active breadcrumb-product" aria-current="page">Экскаватор погрузчик MST M642
                        Plus</li>
                </ol>
            </nav>
            <div className="product-content-container">
                <ProductComponent product={product}></ProductComponent>
            </div>
        </main>
    );
};

export default Product;
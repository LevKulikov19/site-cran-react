import React from 'react';
import classes from './Home.module.css';
import CarouselComponent from '../components/CarouselComponent/CarouselComponent';
import HomeAboutUsComponent from '../components/HomeAboutUsComponent/HomeAboutUsComponent';
import HomeMSTComponent from '../components/HomeMSTComponent/HomeMSTComponent';
import HomeBenefitsComponent from '../components/HomeBenefitsComponent/HomeBenefitsComponent';
import HomeProductCardComponent from '../components/HomeProductCardComponent/HomeProductCardComponent';

const carouselItems = [
    {
        img: "/file/1.png",
        link: "category/special-equipment/1",
        title: "Спецтехника"
    },
    {
        img: "/file/2.png",
        link: "category/attachments/1",
        title: "Навесное оборудование"
    },
    {
        img: "/file/3.png",
        link: "category/road-equipment/1",
        title: "Дорожное оснащение"
    },
    {
        img: "/file/4.png",
        link: "category/materials/1",
        title: "Материалы"
    }
]

const aboutUsTitle = "Компания «Кран» - ваш надежный поставщик спецтехники и дорожного оборудования";
const aboutUsDiscription = 
'На нашем гипермаркете "Велес" вы сможете найти и приобрести специальную технику от лидирующих мировых' +
'производителей: Caterpillar, Kaiser, Komatsu, Liebherr, Menzi Muck и многих других. Доставка осуществляется из' +
'Китая, Кореи, Германии, Италии и США в любую точку России.' +
'Кроме собственных предложений, мы также предлагаем всем собственникам и производителям разместить объявления о' +
'продаже на нашем маркетплейсе.' +
'Наш специализированный маркетплейс ТСК "Велес" уже более 10 лет успешно работает на рынке продажи различных' +
'типов техники и оборудования, предоставляя своим клиентам возможность совершать выгодные и безопасные покупки со' +
'всего мира.';

const MSTTitle = "Экскаваторы погрузчики MST";
const MSTDiscription = 
"Компания Кран рада предложить вам возможность приобрести экскаваторы-погрузчики от официального дилера MST в" +
"России. В нашем ассортименте представлено 8 различных моделей с разными модификациями. Мы предлагаем как" +
"абсолютно новые экскаваторы-погрузчики с официальной гарантией производителя, так и те машины, которые ранее" +
"находились в эксплуатации, но прошли тщательную предпродажную подготовку в нашей сервисной службе." +
"Мы осуществляем продажу экскаваторов-погрузчиков MST на всей территории Российской Федерации.";
const MSTImg = "file/5.png";

const benefitsItems = [
    {
        img: "/file/icon-a-1.png",
        discription: "Более чем 20-летний опыт",
        title: "Опыт работы"
    },
    {
        img: "/file/icon-a-2.png",
        discription: "Максимальная оперативность исполнения заказа. Отсутствие ограничений по объёму заказа.",
        title: "Оперативность"
    },
    {
        img: "/file/icon-a-3.png",
        discription: "Индивидуальные или стандартные технические решения, в зависимости от пожеланий заказчика.",
        title: "Индивидуальный подход"
    },
    {
        img: "/file/icon-a-4.png",
        discription: "Профессиональная техническая поддержка 24x7",
        title: "Техподдержка"
    }
]

const Home = () => {
    window.scrollTo(0, 0);
    return (
        <main className="container-xl container-main">
            <h1 className={classes['main-title'] + ' font-weight-light'}>Продажа строительной техники, дорожных материалов и навесного оборудования
                по России</h1>
            <CarouselComponent items={carouselItems} ></CarouselComponent>
            <HomeAboutUsComponent title={aboutUsTitle} discription={aboutUsDiscription}></HomeAboutUsComponent>
            <HomeMSTComponent title={MSTTitle} discription={MSTDiscription} img={MSTImg}></HomeMSTComponent>
            <HomeBenefitsComponent items={benefitsItems} ></HomeBenefitsComponent>
            <HomeProductCardComponent></HomeProductCardComponent>
        </main>
    );
};

export default Home;
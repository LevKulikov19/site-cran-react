import React from 'react';
import CardComponent from '../components/CardComponent/CardComponent';

const Cart = () => {
    window.scrollTo(0, 0);
    return (
        <main className="container-xl">
            <h1 className="m-3 text-center">Корзина</h1>
            <CardComponent></CardComponent>
        </main>
    );
};

export default Cart;
import React from 'react';
import { Link } from "react-router-dom";
import classes from './Category.module.css';

const Category = () => {
    window.scrollTo(0, 0);
    return (
        <main className="container-xl p-3">
            <nav className="breadcrumb-main">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><Link to="/">Главная</Link></li>
                    <li className="breadcrumb-item active" aria-current="page">Каталог</li>
                </ol>
            </nav>
            <div>
                <div className="row justify-content-center">
                    <Link className={classes['category-item'] + ' text-black'} to="/catalog/special-equipment/1">
                        <img src="/file/category-special-equipment-xl.png" alt="Спецтехника образец" className={classes['header-category-item"']} />
                        <span className={classes['header-category-item"']}>Спецтехника</span>
                    </Link>
                    <Link className={classes['category-item'] + ' text-black'} to="/catalog/special-attachments/1">
                        <img src="/file/category-attachments-xl.png" alt="Навесное оборудование образец" className={classes['header-category-item"']} />
                        <span className={classes['header-category-item"']}>Навесное оборудование</span>
                    </Link>
                </div>
                <div className="row justify-content-center">
                    <Link className={classes['category-item'] + ' text-black'} to="/catalog/road-equipment/1">
                        <img src="/file/category-road-equipment-xl.png" alt="Дорожное оснащение образец" className={classes['header-category-item"']} />
                        <span className={classes['header-category-item"']}>Дорожное оснащение</span>
                    </Link>
                    <Link className={classes['category-item'] + ' text-black'} to="/catalog/materials/1">
                        <img src="/file/category-materials-xl.png" alt="Материалы оборудование образец" className={classes['header-category-item"']} />
                        <span className={classes['header-category-item"']}>Материалы</span>
                    </Link>
                </div>
            </div>
        </main>
    );
};

export default Category;
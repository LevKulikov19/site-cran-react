import React from 'react';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { getCategoryById, getAllByCategory, getCountByCategory } from '../product/ProductService';
import classes from './Catalog.module.css';
import ProductCardGridComponent from '../components/ProductCardGridComponent/ProductCardGridComponent';
import PaginationComponent from '../components/UI/PaginationComponent/PaginationComponent';

const Catalog = () => {
    window.scrollTo(0, 0);
    const params = useParams();
    const row = 2;
    const columns = 4;
    const countPage = Math.ceil(getCountByCategory(params.category) / row / columns);
    let category = getCategoryById(params.category);
    let products = getAllByCategory(category.CategoryID, row*columns, params.page)
    console.log(countPage, getCountByCategory(params.category));
    return (
        <main className={classes['container-category']}>
            <h1 className={classes['h1-product-category'] + ' text-center m-5 fw-light'}>{category.Category}</h1>
            <nav className="breadcrumb-main">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><Link to="/">Главная</Link></li>
                    <li className="breadcrumb-item"><Link to="/category">Каталог</Link></li>
                    <li className="breadcrumb-item active breadcrumb-product-category" aria-current="page">{category.Category}</li>
                </ol>
            </nav>
            <div className={classes['catalog-content-conteiner']}>
                <ProductCardGridComponent products={products} rows={row} columns={columns}></ProductCardGridComponent>
                {   
                    countPage !== 1 ? <PaginationComponent count={countPage} active={params.page} onPageChange={()=>{console.log('change')}} path = {`/catalog/${params.category}/`}></PaginationComponent> : ''
                }
                
            </div>

        </main>
    );
};

export default Catalog;
import React from 'react';
import classes from './Contact.module.css';

const Contact = () => {
    window.scrollTo(0, 0);
    return (
        <main className="container-xl">
            <h1 className="p-3 text-center">Контакты</h1>
            <div className={classes['contact-container'] + ' row justify-content-center p-3'}>
                <div className={classes['contact-container-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Общие вопросы</h5>
                    <ul className={classes['contact-list'] + ' ' + classes['sub-list']  + ' flex-column'}>
                        <li className="mb-2">
                            <img src="/file/email.svg" alt="" />
                            <a href="mailto:market@info-sst.ru" className="nav-link text-dark">market@info-sst.ru</a>
                        </li>
                        <li className="mb-2">
                            <img src="/file/vk.svg" alt="" />
                            <a href="https://vk.com/" className="nav-link text-dark">ВКонтакте</a>
                        </li>
                        <li className="mb-2">
                            <img src="/file/youtube.svg" alt="" />
                            <a href="https://www.youtube.com/" className="nav-link text-dark">Youtube</a>
                        </li>
                    </ul>
                </div>

                <div className={classes['contact-container-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Гипермаркет "Кран"</h5>
                    <ul className={classes['sub-list'] + ' flex-column'}>
                        <li className="mb-2"><span>ООО КРАН</span></li>
                        <li className="mb-2"><span>ИНН 4027144871 / КПП 402701001</span></li>
                        <li className="mb-2"><span>ОГРН 1204000009531</span></li>
                    </ul>
                </div>
            </div>
        </main>
    );
};

export default Contact;
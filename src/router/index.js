import AboutUs from "../pages/AboutUs";
import Cart from "../pages/Cart";
import Catalog from "../pages/Catalog";
import Category from "../pages/Category";
import Contact from "../pages/Contact";
import Home from "../pages/Home";
import Product from "../pages/Product";

export const routes = [
    {path: '/', component: <Home/>, exact: true},
    {path: '/about-us', component: <AboutUs/>, exact: true},
    {path: '/cart', component: <Cart/>, exact: true},
    {path: '/contact', component: <Contact/>, exact: true},
    {path: '/category', component: <Category/>, exact: true},
    {path: '/catalog/:category/:page', component: <Catalog/>, exact: true},
    {path: '/product/:id', component: <Product/>, exact: true},
]

import React from 'react';
import classes from './HomeBenefitsComponent.module.css';

const HomeBenefitsItemComponent = ({ title, discription, img }) => {
    return (
        <div className={classes['main-block-3-card']}>
          <img src={img} alt={title} />
          <h3>{title}</h3>
          <span>{discription}</span>
        </div>
    );
};

export default HomeBenefitsItemComponent;
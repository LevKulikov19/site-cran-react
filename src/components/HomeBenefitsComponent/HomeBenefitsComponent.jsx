import React from 'react';
import classes from './HomeBenefitsComponent.module.css';
import HomeBenefitsItemComponent from './HomeBenefitsItemComponent';

const HomeBenefitsComponent = ({ items }) => {
    return (
        <section className={classes['main-block-3'] + ' row'}>
            {items.map((item, index) =>
                <HomeBenefitsItemComponent title={item.title} discription={item.discription} img={item.img} key={index} />
            )}
        </section>
    );
};

export default HomeBenefitsComponent;
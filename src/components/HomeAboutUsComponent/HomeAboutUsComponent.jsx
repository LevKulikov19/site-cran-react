import React from 'react';
import classes from './HomeAboutUsComponent.module.css';

const HomeAboutUsComponent = ({ title, discription }) => {
    return (
        <section className="main-block-1">
            <h2 className={classes['main-title'] + ' font-weight-light'}>{title}</h2>
            <p>{discription}</p>
        </section>
    );
};

export default HomeAboutUsComponent;
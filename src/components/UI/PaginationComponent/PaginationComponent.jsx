import React from 'react';
import classes from './PaginationComponent.module.css';
import { Link } from 'react-router-dom';


const PaginationComponent = ({ count, active, onPageChange, path }) => {
    const pages = [];
    for (let i = 1; i <= count; i++) {
        let style = `page-item ${i === parseInt(active) ? 'disabled' : ''}`;
        pages.push(
            <li className={style} key={i}>
                <Link className="page-link text-warning" to={path + `${i}`} onClick={() => onPageChange(i)}>
                    {i}
                </Link>
            </li>
        );
    }

    return (
        <nav className={classes['pagination-category']}>
            <ul className="pagination justify-content-center">
                {pages}
            </ul>
        </nav>
    );
};

export default PaginationComponent;

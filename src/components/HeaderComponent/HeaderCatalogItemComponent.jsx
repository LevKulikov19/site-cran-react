import React from 'react';
import { Link } from "react-router-dom";
import classes from './HeaderComponent.module.css';

const HeaderCatalogItemComponent = (props) => {
    return (
        <Link className={classes['category-item'] + ' dropdown-item'} to={props.link}>
            <img src={props.imgSrc} alt={props.imgAlt}
                className={classes['category-item']} />
            <span className={classes['category-item']}>{props.name}</span>
        </Link>
    );
};

export default HeaderCatalogItemComponent;

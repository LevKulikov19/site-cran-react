import React, { useContext } from 'react';
import { Link } from "react-router-dom";
import classes from './HeaderComponent.module.css';
import HeaderCatalogItemComponent from './HeaderCatalogItemComponent';
import { CartContext } from "../../context/CartContext";

const HeaderComponent = () => {
    const { getItems } = useContext(CartContext);
    let products = getItems();
    let imgPath = products.length > 0 ? "/file/icon-card-active.svg" : "/file/icon-card.svg";
    return (
        <header className="p-3 text-bg-dark">
            <div className={classes['main'] + ' container'}>
                <div className={classes['header-category']}>
                    <div className="dropdown">
                        <button className={classes['category-button'] + ' btn btn-secondary dropdown-toggle'} type="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="/file/icon-burger-menu.svg" alt="" className={classes['category-button']} />
                            <span className="text-light" >Категории</span>
                        </button>

                        <div className={classes['category-contaier'] + ' dropdown-menu'}>
                            <div className={classes['category-row']}>
                                <HeaderCatalogItemComponent name="Спецтехника"
                                    imgSrc="/file/category-special-equipment.png" imgAlt="Спецтехника образец"
                                    link="catalog/special-equipment/1" />
                                <HeaderCatalogItemComponent name="Навесное оборудование"
                                    imgSrc="/file/category-attachments.png" imgAlt="Навесное оборудование образец"
                                    link="catalog/special-attachments/1" />
                            </div>
                            <div className={classes['category-row']}>
                                <HeaderCatalogItemComponent name="Дорожное оснащение"
                                    imgSrc="/file/category-road-equipment.png" imgAlt="Дорожное оснащение образец"
                                    link="catalog/road-equipment/1" />
                                <HeaderCatalogItemComponent name="Материалы"
                                    imgSrc="/file/category-materials.png" imgAlt="Материалы оборудование образец"
                                    link="catalog/materials/1" />
                            </div>
                        </div>
                    </div>
                </div>
                <Link to="/" className={classes['link-home']}><img src="/file/logo.svg" alt="логотип" className={classes['logo']} /></Link>
                <Link to="/cart" className={classes['card']}><img src={imgPath} alt="корзина" className={classes['card']} /></Link>
            </div>
            <nav className={classes['menu'] + ' navbar navbar-dark bg-dark'}>
                <div className="container-fluid">
                    <button className={classes['navbar-toggler-mobile'] + ' navbar-toggler'} type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarsExample01">
                        <ul className="navbar-nav me-auto mb-2">
                            <li className="nav-item">
                                <Link className="nav-link" to="category">Каталог</Link>
                            </li>
                            <ul className="me-auto mb-2 sub-list">
                                <li className="nav-item">
                                    <Link className="nav-link" to="catalog/special-equipment/1">Спецтехника</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="catalog/special-attachments/1">Навесное оборудование</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="catalog/road-equipment/1">Дорожное оснащение</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" to="catalog/materials/1">Материалы</Link>
                                </li>
                            </ul>
                            <li className="nav-item">
                                <Link className="nav-link" to="about-us">О нас</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="contact">Контакты</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navbar-expand-md navbar-dark navbar-main">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse justify-content-md-center">
                        <ul className="navbar-nav">
                            <li className={classes['nav-item-padding'] + ' nav-item'}>
                                <Link className="nav-link" to="category">Весь каталог</Link>
                            </li>
                            <li className={classes['nav-item-padding'] + ' nav-item'}>
                                <Link className="nav-link" to="about-us">О нас</Link>
                            </li>
                            <li className={classes['nav-item-padding'] + ' nav-item'}>
                                <Link className="nav-link" to="contact">Контакты</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );
};

export default HeaderComponent;
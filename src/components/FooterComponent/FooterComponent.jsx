import React from 'react';
import { Link } from "react-router-dom";
import classes from './FooterComponent.module.css';

const FooterComponent = () => {
    return (
        <footer className="py-5 bg-dark text-white btn-outline-light form-white container-fluid">
            <div className="row justify-content-center">
                <div className={classes['footer-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Информация</h5>
                    <ul className="nav flex-column">
                        <li className="nav-item mb-2"><Link to="/" className="nav-link p-0 text-light">Главаня</Link></li>
                        <li className="nav-item mb-2"><Link to="/contact" className="nav-link p-0 text-light">Контакты</Link></li>
                        <li className="nav-item mb-2"><Link to="/about-us" className="nav-link p-0 text-light">О нас</Link></li>
                        <li className="nav-item mb-2"><Link to="/cart" className="nav-link p-0 text-light">Корзина</Link></li>
                    </ul>
                </div>

                <div className={classes['footer-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Каталог</h5>
                    <ul className="nav flex-column">
                        <li className="nav-item mb-2"><Link to="/catalog/special-equipment/1"
                            className="nav-link p-0 text-light">Спецтехника</Link></li>
                        <li className="nav-item mb-2"><Link to="/catalog/attachments/1" className="nav-link p-0 text-light">Навесное
                            оборудование</Link></li>
                        <li className="nav-item mb-2"><Link to="/catalog/road-equipment/1" className="nav-link p-0 text-light">Дорожное
                            оборудование</Link></li>
                        <li className="nav-item mb-2"><Link to="/catalog/road-equipment/1" className="nav-link p-0 text-light">Материалы</Link>
                        </li>
                    </ul>
                </div>

                <div className={classes['footer-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Контатная информация</h5>
                    <ul className="nav flex-column">
                        <li className="nav-item mb-2"><Link to="tel: +79109208296" className="nav-link p-0 text-light">Телефон:
                            +79109208296</Link></li>
                        <li className="nav-item mb-2"><Link to="mailto: market@info-sst.ru"
                            className="nav-link p-0 text-light">market@info-sst.ru</Link></li>
                        <li className={classes['footer-inline'] + ' nav-item mb-2'}>
                            <Link to="https://vk.com/" className="nav-link p-1 text-light">
                                <img src="/file/vk-c.svg" alt="" className={classes['footer-vk-icon']} />
                            </Link>
                            <Link to="https://www.youtube.com/" className="nav-link p-1 text-light">
                                <img src="/file/youtube-c.svg" alt="" className={classes['footer-youtube-icon']} />
                            </Link>
                        </li>
                    </ul>
                </div>

                <div className={classes['footer-item'] + ' col-6 col-md-2 mb-3'}>
                    <h5>Скачать наше приложение</h5>
                    <ul className="nav flex-column">
                        <li className="nav-item mb-2"><Link to="https://www.apple.com/ru/Linkpp-store/" className="nav-link p-0 text-light">
                            <img src="/file/app-store.svg" alt="" className="app-store" />
                        </Link></li>
                        <li className="nav-item mb-2"><Link to="https://play.google.com/store/games?hl=ru&gl=US"
                            className="nav-link p-0 text-light">
                            <img src="/file/google-play.svg" alt="" className="google-play" />
                        </Link></li>
                    </ul>
                </div>
                <div className="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top">
                    <p>&copy; ТСК «Велес», 2013—2023</p>
                    <Link to="#" className="nav-link p-0 text-light">Политика конфиденциальности</Link>
                </div>
            </div>


        </footer>
    );
};

export default FooterComponent;
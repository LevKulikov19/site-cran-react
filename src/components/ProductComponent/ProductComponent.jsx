import React, {useContext} from 'react';
import classes from './ProductComponent.module.css';
import formatPrice from '../../utils/formatPrice.js';
import { Link } from 'react-router-dom';
import { CartContext  } from "../../context/CartContext"; 

const ProductComponent = ({ product }) => {
    const { addItem } = useContext(CartContext);
    const [quantity, setQuantity] = React.useState(1); 
    return (
        <div className={classes['product-main-base-container'] + ' row justify-content-center'}>
            <div className={classes['product-img-container']}>
                <img src={product.URLImage} />
            </div>
            <div className={classes['product-container']}>
                <div className={classes['product-category']}>
                    <img src="/file/icon-tag.svg" />
                    <span>{product.Category}</span>
                </div>
                <h1 className={classes['product-name'] + ' fw-light'}>{product.Name}</h1>
                <p className={classes['product-price'] + ' fw-light fs-2'}>{formatPrice(product.Price)}</p>
                <div className={classes['product-ttx-container'] + ' row justify-content-between'}>
                    <div className={classes['product-ttx']}>
                        <p>{product.TTX1}</p>
                        <p>{product.TTX2}</p>
                    </div>
                    <div className={classes['product-ttx-other-container']}>
                        <p className={classes['product-brand']}><span>Бренд: </span>{product.Brand}</p>
                        <p className={classes['product-code']}><span>Код товара: </span>#{product.ID}</p>
                        <p className={classes['product-count']}><span>Наличие: </span>{product.Count} штук</p>
                    </div>
                </div>
                <div className={classes['product-action']}>
                    <div className={classes['product-action-container']}>
                        <span>Количество</span>
                        <input value={quantity} type="number" min="1" max="100" defaultValue="1" onChange={(e) => setQuantity(e.target.value)} />
                    </div>
                    <Link to="/cart">
                        <button type="button" className="btn btn-warning text-white" onClick={()=>{addItem(product, quantity);}}>Купить</button>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default ProductComponent;

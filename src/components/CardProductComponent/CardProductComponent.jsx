import React from 'react';
import { Link } from "react-router-dom";
import formatPrice from '../../utils/formatPrice';
import classes from './CardProductComponent.module.css';

const CardProductComponent = ({ product, onQuantityChange, onDelete }) => {
    console.log(product)
    
    const cost = product.Price * product.Quantity;
    console.log(cost, "a")
    console.log(product.Price)
    return (
        <tr className={classes['table-td']}>
            <th scope="row">
                <Link to={`/product/${product.ID}`} className={classes['table-product']}>
                    <img src={product.URLImage} alt="" className={classes['table-product-img']} />
                    <p className={classes['table-product-p'] + ' text-black'}>{product.Name}</p>
                </Link>
            </th>
            <td className={classes['table-product-quantity'] + ' ' + classes['table-td'] }>
                <button className={classes['card-quantity-action-d'] + ' ' + classes['card-quantity-lower'] + ' ' + classes['delete-item-btn']}
                    onClick={() => onQuantityChange(product, -1)}>-</button>
                <span>{product.Quantity}</span>
                <button className={classes['card-quantity-action-d'] + ' ' + classes['card-quantity-upper'] + ' ' +  classes['delete-item-btn']}
                    onClick={() => onQuantityChange(product, 1)}>+</button>
            </td>
            <td className={classes['table-product-price']}>{formatPrice(product.Price)}</td>
            <td className={classes['table-product-cost']}>{formatPrice(cost)}</td>
            <td className={classes['table-delete-item']}>
                <button type="button" className={classes['delete-item-btn']} onClick={() => onDelete(product)}>×</button>
            </td>
        </tr>
    );
};

export default CardProductComponent;

import React from 'react';
import classNamees from './CarouselComponent.module.css';
import CarouseltemComponent from './CarouseltemComponent';

const CarouselComponent = ({ items }) => {
    return (
        <div className={classNamees['carousel-main'] + ' carousel slide carousel-dark'} data-bs-ride="carousel" id="carouselExample">
            <div className="carousel-inner">
                {items.map((item, index) =>
                    <CarouseltemComponent data={item} active={index === 0} key={index} />
                )}
            </div>
            <button className={classNamees['carousel-main-control-prev'] + ' carousel-control-prev'} type="button" data-bs-target="#carouselExample"
                data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className={classNamees['carousel-main-control-next'] + ' carousel-control-next'} type="button" data-bs-target="#carouselExample"
                data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    );
};

export default CarouselComponent;
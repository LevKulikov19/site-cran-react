import React from 'react';
import { Link } from "react-router-dom";
import classes from './CarouselComponent.module.css';

const CarouseltemComponent = ({data, active}) => {
    return (
        <div className={active ? classes['carousel-main-item'] + ' carousel-item active' : classes['carousel-main-item'] + ' carousel-item' }>
            <Link to={data.link}>
                <img src={data.img} className="d-block w-100" alt={data.title} />
                <span className="font-weight-light text-white">{data.title}</span>
            </Link>
        </div>
    );
};

export default CarouseltemComponent;

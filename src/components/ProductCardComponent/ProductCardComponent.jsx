import React, {useContext} from 'react';
import formatPrice from '../../utils/formatPrice';
import ProductCardModalComponent from '../ProductCardModalComponent/ProductCardModalComponent';
import MyModal from '../UI/MyModal/MyModal';
import classes from './ProductCardComponent.module.css';
import { Link } from 'react-router-dom';
import { CartContext  } from "../../context/CartContext"; 

const ProductCardComponent = ({ product }) => {
    const [modal, setModal] = React.useState(false);

    const { addItem } = useContext(CartContext);
    

    return (
        <div className={classes['main-card'] + ' card'}>
            <div className={classes['main-card-body'] + ' card-body'}>
                <img onClick={() => { setModal(true) }}
                    src={product.URLImage}
                    className={classes['main-card-img'] + ' card-img-top'}
                />
                <span className={classes['main-card-category']}>{product.Category}</span>
                <Link to={`/product/${product.ID}`} className={classes['main-card-name'] + ' text-black'}>
                    <h5>{product.Name}</h5>
                </Link>
                <div className={classes['main-card-ttx']}>
                    <p>{product.TTX1}</p>
                    <p>{product.TTX2}</p>
                </div>
                <span className={classes['main-card-price']}>{formatPrice(product.Price)}</span>
            </div>
            <MyModal visible={modal} setVisible={setModal}>
                <ProductCardModalComponent product={product} setVisible={setModal}  ></ProductCardModalComponent>
            </MyModal>
        </div>
    );
};

export default ProductCardComponent;

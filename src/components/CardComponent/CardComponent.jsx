import React, { useContext } from 'react';
import CardProductComponent from '../CardProductComponent/CardProductComponent';
import CardProductMobileComponent from '../CardProductMobileComponent/CardProductMobileComponent';
import classes from './CardComponent.module.css';
import { CartContext } from "../../context/CartContext";
import formatPrice from '../../utils/formatPrice';



const CardComponent = () => {
    const { getItems, clearCart, removeItem, updateQuantity } = useContext(CartContext);

    const onDeleteAll = () => {
        clearCart();
    }

    const onDelete = (product) => {
        removeItem(product.ID);
    }

    const onQuantityChange = (product, count) => {
        if (product.Quantity+count <= 0) updateQuantity(product.ID, 1);
        else if (product.Quantity+count >= product.Count) updateQuantity(product.ID, product.Count);
        else updateQuantity(product.ID, product.Quantity + count);
    }

    const onBuy = () => {
        clearCart();
    }
    let products = getItems();
    let cost = 0;
    products.forEach(element => {
        cost += element.Quantity * element.Price;
    });

    if (products.length === 0) {
        return (<h2 className="m-3 text-center card-empty">Пока не был добавлен ни один товар</h2>)
    }
    else {

        return (
            <div>

                <button type="button" className={classes['table-delete-item-mobile'] + ' btn btn-danger text-white'} onClick={onDeleteAll}>Удалить все</button>
                <table className={classes['table'] + ' table'}>
                    <thead>
                        <tr>
                            <th scope="col">Товар</th>
                            <th scope="col">Количество</th>
                            <th scope="col">Цена</th>
                            <th scope="col">Стоимость</th>
                            <th scope="col" className={classes['table-delete-item']}>
                                <button type="button" className="btn btn-danger text-white" onClick={onDeleteAll}>Удалить все</button>
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody-main">
                        {products.map((product, index) =>
                            <CardProductComponent key={index} product={product} onDelete={onDelete} onQuantityChange={onQuantityChange}></CardProductComponent>
                        )}
                    </tbody>
                </table>
                <div className={classes['table-mobile']}>
                    {products.map((product, index) =>
                        <CardProductMobileComponent key={index} product={product}></CardProductMobileComponent>
                    )}
                </div>
                <div className={classes['card-end-block']}>
                    <p className="cart-cost">{formatPrice(cost)}</p>
                    <button type="button" className="btn btn-warning text-white cart-btn" onClick={onBuy}>Купить</button>
                </div>
            </div>
        );
    }
};

export default CardComponent;

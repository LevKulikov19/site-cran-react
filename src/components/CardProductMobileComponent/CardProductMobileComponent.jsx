import React from 'react';
import { Link } from "react-router-dom";
import formatPrice from '../../utils/formatPrice';
import classes from './CardProductMobileComponent.module.css';

const CardProductMobileComponent = ({ product, onQuantityChange, onDelete }) => {
    return (
        <div className={classes['table-item-mobile']}>
            <div className={classes['table-product-mobile']}>
                <Link to={`/product/${product.ID}`}>
                    <img src={product.UrlImage} alt="" className={classes['table-product-img']} />
                    <p className={classes['table-product-p-mobile'] + ' text-black'}>{product.Name}</p>
                </Link>
                <button type="button" className={classes['table-delete-item'] + ' ' + classes['delete-item-btn']} onClick={() => onDelete(product)}>×</button>
            </div>
            <div className={classes['table-product-data']}>
                <p>Количество</p>
                <button className={classes['card-quantity-action'] + ' ' + classes['card-quantity-lower'] + ' ' + classes['delete-item-btn']}
                    onClick={() => onQuantityChange(product, -1)}>-</button>
                <span>{product.Quantity}</span>
                <button className={classes['card-quantity-action'] + ' ' + classes['card-quantity-upper'] + ' ' + classes['delete-item-btn']}
                    onClick={() => onQuantityChange(product, 1)}>+</button>
            </div>
            <div className={classes['table-product-data']}>
                <p>Цена</p>
                <span className={classes['table-product-price']}>{formatPrice(product.Price)}</span>
            </div>
            <div className={classes['table-product-data']}>
                <p>Стоимость</p>
                <span className={classes['table-product-cost']}>{formatPrice(product.Cost)}</span>
            </div>
        </div>
    );
};

export default CardProductMobileComponent;

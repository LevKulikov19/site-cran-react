import React from 'react';
import classes from './HomeProductCardComponent.module.css';
import { getById } from '../../product/ProductService';
import ProductCardComponent from '../ProductCardComponent/ProductCardComponent';

const product1 = getById(1);
const product2 = getById(2);
const product3 = getById(3);
const product4 = getById(4);

const HomeProductCardComponent = () => {
    return (
        <section className={classes['main-block-4']}>
            <h2 className={classes['main-block-4-title']}>Популярная спецтехника от производителя</h2>
            <div className={'row justify-content-center'}>
                <ProductCardComponent product={product1}></ProductCardComponent>
                <ProductCardComponent product={product2}></ProductCardComponent>
                <ProductCardComponent product={product3}></ProductCardComponent>
                <ProductCardComponent product={product4}></ProductCardComponent>
            </div>
        </section>
    );
};

export default HomeProductCardComponent;
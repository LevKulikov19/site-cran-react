import React from 'react';
import classes from './HomeMSTComponent.module.css';

const HomeMSTComponent = ({ title, discription, img }) => {
    return (
        <section className={classes['main-block-2']}>
            <div className={classes['main-block-2-container']}>
                <h2>{title}</h2>
                <p>{discription}</p>
            </div>
            <img src={img} alt={title} />
        </section>
    );
};

export default HomeMSTComponent;
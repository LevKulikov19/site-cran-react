import React, {useContext} from 'react';
import formatPrice from '../../utils/formatPrice.js';
import classes from './ProductCardModalComponent.module.css';
import { Link } from 'react-router-dom';
import { CartContext  } from "../../context/CartContext"; 

const ProductCardModalComponent = ({ product, setVisible, addToCard }) => {
    const { addItem } = useContext(CartContext);
    const [quantity, setQuantity] = React.useState(1); 
    return (
        <div className={classes['main-modal'] + ' modal-dialog'}>
            <div className={classes['main-modal-content'] + ' modal-content p-3'}>
                <div className={classes['main-modal-content-contaiter']}>
                    <div className={classes['main-modal-content-1']}>
                        <img src={product.URLImage} className={classes['modal-main-img']} />
                    </div>
                    <div className={classes['main-modal-content-2']} >
                        <h2>{product.Name}</h2>
                        <p className={classes['main-modal-price']}>{formatPrice(product.Price)}</p>
                        <div className={classes['modal-main-ttx-container']}>
                            <div className={classes['modal-main-ttx']}>
                                <h6 className={classes['modal-main-ttx-title']}>Характеристики</h6>
                                <div className={classes['modal-main-ttx-text']}>{product.TTX1}</div>
                                <div className={classes['modal-main-ttx-text']}>{product.TTX2}</div>
                            </div>
                            <div className="modal-main-ttx">
                                <h6 className={classes['modal-main-ttx-title']}>Бренд</h6>
                                <div className={classes['modal-main-ttx-text']}>{product.Brand}</div>
                            </div>
                            <div className="modal-main-ttx">
                                <h6 className={classes['modal-main-ttx-title']}>Код товара</h6>
                                <div className={classes['modal-main-ttx-text']}>#{product.ID}</div>
                            </div>
                            <div className="modal-main-ttx">
                                <h6 className={classes['modal-main-ttx-title']}>Количество</h6>
                                <div className={classes['modal-main-ttx-text']}>{product.Count} штук</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={classes['modal-main-action']}>
                    <div className={classes['modal-main-action']}>
                        <div className={classes['modal-main-count-container']}>
                            <span>Количество</span>
                            <input type="number" min="1" max="100" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                        </div>
                        <div className={classes['modal-main-action-container']}>
                            <Link to="/cart">
                                <button type="button" className="btn btn-warning text-white" onClick={()=>{addItem(product, quantity);}} >Купить</button> 
                            </Link>
                            <Link to={`/product/${product.ID}`}>
                                <button type="button" className="btn btn-warning text-white">Подробнее</button>
                            </Link>
                        </div>
                    </div>
                </div>
                <button type="button" className={classes['main-modal-close']} onClick={() => setVisible(false)}>×</button>
            </div>
        </div>
    );
};

export default ProductCardModalComponent;

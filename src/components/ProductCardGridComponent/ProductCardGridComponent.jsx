import React from 'react';
import ProductCardComponent from '../ProductCardComponent/ProductCardComponent';
import classes from './ProductCardGridComponent.module.css';

const ProductCardFlexComponent = ({ products, rows, columns }) => {
    const flexContainerStyle = {
        display: 'flex',
        flexWrap: 'wrap'
    };
    const flexItemStyle = {
        flex: `0 0 calc(100% / ${rows})`
    };

    const grid = (products, rows, columns) =>{     
        let matrix = [];
        for (let i = 0; i < rows; i++) {
            let row = [];
            for (let j = 0; j < columns; j++) {
                if (products[i * columns + j] == undefined) {
                    break;
                }
                row.push(products[i * columns + j]);
            }
            matrix.push(row);
        }
    
        return matrix;
    };
    const chunkedProducts = grid(products, rows, columns);
    console.log(chunkedProducts)

    return (
        <div className={classes['catalog-content-conteiner']}>
            {chunkedProducts.map((productChunk, index) => 
                <div key={index} className="row justify-content-center">
                    {productChunk.map((product, index) => 
                            <ProductCardComponent product={product} key={index} />
                    )}
                </div>
            )}
        </div>
    );
};

export default ProductCardFlexComponent;

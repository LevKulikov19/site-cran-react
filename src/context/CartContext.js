import React, { createContext, useReducer, useEffect } from 'react';

// Определение действий
const ACTIONS = {
    ADD_ITEM: 'add-item',
    REMOVE_ITEM: 'remove-item',
    UPDATE_QUANTITY: 'update-quantity',
    CLEAR_CART: 'clear-cart'
};

// Создание контекста
const CartContext = createContext();

// Определение редьюсера
function cartReducer(state, action) {
    switch (action.type) {
        case ACTIONS.ADD_ITEM:
            const existingItemIndex = state.findIndex(item => item.ID === action.payload.ID);
            if (existingItemIndex >= 0) {
                const newState = [...state];
                newState[existingItemIndex].Quantity += action.payload.Quantity;
                return newState;
            } else {
                return [...state, action.payload];
            }
        case ACTIONS.REMOVE_ITEM:
            return state.filter(item => item.ID !== action.payload.ID);
        case ACTIONS.UPDATE_QUANTITY:
            return state.map(item => item.ID === action.payload.ID ? { ...item, Quantity: action.payload.Quantity } : item);
        case ACTIONS.CLEAR_CART:
            return [];
        default:
            return state;
    }
}

// Создание провайдера
function CartProvider({ children }) {
    const [cart, dispatch] = useReducer(cartReducer, [], () => {
        const localData = localStorage.getItem('cart');
        return localData ? JSON.parse(localData) : [];
    });

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart));
    }, [cart]);

    function addItem(item, Quantity) {
        dispatch({ type: ACTIONS.ADD_ITEM, payload: { ...item, Quantity } });
    }

    function removeItem(ID) {
        dispatch({ type: ACTIONS.REMOVE_ITEM, payload: { ID } });
    }

    function updateQuantity(ID, Quantity) {
        dispatch({ type: ACTIONS.UPDATE_QUANTITY, payload: { ID, Quantity } });
    }

    function clearCart() {
        dispatch({ type: ACTIONS.CLEAR_CART });
    }

    function getItems() {
        return cart;
    }

    return (
        <CartContext.Provider value={{ cart, addItem, removeItem, updateQuantity, clearCart, getItems }}>
            {children}
        </CartContext.Provider>
    );
}

export { CartContext, CartProvider };
